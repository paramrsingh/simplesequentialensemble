import functions_framework
from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
from typing import Dict


def predict_tabular_classification_sample(
    project: str,
    endpoint_id: str,
    instance_dict: Dict,
    location: str = "us-central1",
    api_endpoint: str = "us-central1-aiplatform.googleapis.com",):
        # The AI Platform services require regional API endpoints.
        client_options = {"api_endpoint": api_endpoint}
        # Initialize client that will be used to create and send requests.
        # This client only needs to be created once, and can be reused for multiple requests.
        client = aiplatform.gapic.PredictionServiceClient(client_options=client_options)
        # for more info on the instance schema, please use get_model_sample.py
        # and look at the yaml found in instance_schema_uri
        instance = json_format.ParseDict(instance_dict, Value())
        instances = [instance]
        parameters_dict = {}
        parameters = json_format.ParseDict(parameters_dict, Value())
        endpoint = client.endpoint_path(
            project=project, location=location, endpoint=endpoint_id
        )
        response = client.predict(
            endpoint=endpoint, instances=instances, parameters=parameters
        )
        # print("response")
        # print(" deployed_model_id:", response.deployed_model_id)
        # See gs://google-cloud-aiplatform/schema/predict/prediction/tabular_classification_1.0.0.yaml for the format of the predictions.
        predictions = response.predictions
        for prediction in predictions:
            return (" prediction:", str(dict(prediction)))

@functions_framework.http
def hello_get(request):
    """HTTP Cloud Function.
    Args:
        request (flask.Request): The request object.
        <https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data>
    Returns:
        The response text, or any set of values that can be turned into a
        Response object using `make_response`
        <https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response>.
    Note:
        For more information on how Flask integrates with Cloud
        Functions, see the `Writing HTTP functions` page.
        <https://cloud.google.com/functions/docs/writing/http#http_frameworks>
    """

    sfactor1Arg = request.get_json().get('sfactor1')
    print(sfactor1Arg)

    # first Vertex Online Endpoint for multi-class classification

    instance={ "Area": "28395",
                "Perimeter": 610.291,
                "MajorAxisLength": 208.1781167,
                "MinorAxisLength": 173.888747,
                "AspectRation": 1.197191424,
                "Eccentricity": 0.5498121871,
                "ConvexArea": "28715",
                "EquivDiameter": 190.1410973,
                "Extent": 0.7639225182,
                "Solidity": 0.9888559986,
                "roundness": 0.9580271263,
                "Compactness": 0.9133577548,
                "ShapeFactor1": sfactor1Arg,
                "ShapeFactor2": 0.003147289167,
                "ShapeFactor3": 0.8342223882,
                "ShapeFactor4": 0.998723889,}
    
    
    try:
        result1 = predict_tabular_classification_sample(
                project="32316944017",
                endpoint_id="3389022491270709248",
                location="us-central1",
                instance_dict=instance)
        print(result1)
    except Exception as e:
        print(e)

    # Here you can take whatever action you want with the response.
    # This sample is just going to call the second one directly

    # second webservice
    instance2={ "Sex": "M",
            "Length": 0.545,
            "Diameter": 0.425,
            "Height": 0.14,
            "Whole_weight": 0.8005,
            "Shucked_weight": 0.336,
            "Viscera_weight": 0.171,
            "Shell_weight": 0.2345,}
    
    try:
        result2 = predict_tabular_classification_sample(
                project="32316944017",
                endpoint_id="5449313767676444672",
                location="us-central1",
                instance_dict=instance2)
        print(result2)
    except Exception as e:
        print(e)

    return 'final result = ' + str(result2)